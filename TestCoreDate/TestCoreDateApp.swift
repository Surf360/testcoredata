//
//  TestCoreDateApp.swift
//  TestCoreDate
//
//  Created by Reto Fabbri on 27.07.21.
//

import SwiftUI

@main
struct TestCoreDateApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
